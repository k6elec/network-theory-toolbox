function [S,freq]=loadS2Pfile(filename)
% loads a touchstone snp file and returns its S-parameters
%
%   [S,freq]=loadS2Pfile(filename)
%
% S is an NxNxF matrix of S-paramters
% freq is an 1xF frequency vector
%
% Adam Cooman, ELEC VUB


hs = sparameters(filename);

S = hs.Parameters;
freq = hs.Frequencies;


end