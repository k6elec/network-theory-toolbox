function [Sp,S,G] = multiportConnection(parts,conn,debug)
% This function connects multiports together to form a port matrix where
% only the unconnected ports are left.
%
%   Sp = multiportConnection(parts,conn)
%
% INPUT:
%   parts   cell array of (MxMxF) S matrices which contain every part. N
%           can differ for different S matrices, but F should always be 
%           the same
%   conn    N x 4 matrix indicating the connection of two ports. If you
%           want to connect port 1 of part 2 to port 3 of part 4, 
%           conn(N,:) looks like [2 1 4 3]. 
%           Each port can only appear once in the list. For parallel
%           connections, add Tee sections.
%
% OUTPUT:
%   Sp      QxQxF S-matrix of the remaining system after connecting the
%           ports together. Q is the amount of ports which are not used in
%           the connection matrix.
%  
%
% The S-matrix of a tee piece is:
%
%    |---------|                   | -1  2  2 |
% o--|------o--|--o            1   |          |
%    |      |  |       =>  S = - * | 2  -1  2 |
% o--|--o------|--o            3   |          |
% 	 |__|___|__|                   | 2   2 -1 |
%       |   |
%       o   o
% 
% Adam Cooman; ELEC VUB
%   26/07/2013  Version 1
%   29/07/2013  added support for multiple frequencies
%   21/08/2014  you can add [N x N x 1] vectors now as well, they will be
%               repeated over all frequencies.
%
% TODO
% - add support for when the ports have a different characteristic
%   impedance. This can be done using the formulas for the G matrix found
%   in Dobrowolski1989

if ~exist('debug','var')
    debug=0;
end

% Check each provided matrix and save their dimension
sizes=zeros(length(parts),1);
Fvector = zeros(length(parts),1);
for ii=1:length(parts)
    [N,M,F]=size(parts{ii});
    Fvector(ii) = F;
    if N~=M
        error('each matrix should be square')
    else
        sizes(ii)=N;
    end
end

% check the frequencies of the parts
F = max(Fvector);
if F~=1
    if any(~or(Fvector==F,Fvector==1))
        error('The amount of frequencies in each part should be either 1 or F');
    else
        inds = find(Fvector==1);
        for ii=1:length(inds)
            parts{inds(ii)} = repmat(parts{inds(ii)},[1 1 F]);
        end
    end
end

% if the sum of the S matrix is bigger than 100, go sparse, otherwise, use normal matrices
if sum(sizes)>200
    sparsemode=1;
    disp('going into sparse mode for the solver')
else
    sparsemode=0;
end


%% Build the big S matrix
if sparsemode
    % SPARSE MODE
    % to allocate a sparse matrix efficiently, we need:
    %   s:  a vector with the nonzero elements of the matrix
    %   i:  the first part of the index where the nonzero element lies
    %   j:  the second part of the index where the nonzero element lies
    
    numberofelements=sum(sizes.^2);
    % preallocate the vectors with nonzero elements
    s=zeros(numberofelements,1);
    i=zeros(numberofelements,1);
    j=zeros(numberofelements,1);
    curr=0;
    curr2=0;
    % build the vectors such that the result will be a block diagonal matrix
    for ii=1:length(sizes)
        % get the info for this specific part
        part=parts{ii};
        N = sizes(ii);
        N2 = N^2;
        % put all the numbers of the port matrix in a vector
        for ff=1:F
            s(curr2+1:curr2+N2,ff)=part(:,:,ff);
        end
        % generate the indices of the vector
        i  (curr2+1:curr2+N2)=curr+repmat((1:N)',N,1);
        j  (curr2+1:curr2+N2)=curr+reshape(repmat(1:N,N,1),[N2,1]);
        curr=curr+N;
        curr2=curr2+N2;
    end
    
    % build the cell array of sparse matrices S
    parfor ff=1:F
        S{ff} = sparse(i,j,s(:,ff));
    end
    
    if debug
        figure
        spy(S{end});
    end

else
    % NORMAL (FULL MATRIX) mode    
    S = zeros(sum(sizes),sum(sizes),F);
    curr=0;
    for ii=1:length(sizes)
        N = sizes(ii);
        S(curr+1:curr+N,curr+1:curr+N,:)=parts{ii};
        curr=curr+N;
    end
    
    if debug
        figure
        spy(S(:,:,end));
    end
    
end

%% Build the connection matrix G
[N,M] = size(conn);

if M~=4
    error('the connection matrix conn should be N x 4');
end

% check whether the ports exist
for ii=1:N
    for jj=1:2:3
        if conn(ii,jj)<=length(parts)
            if conn(ii,jj+1)>sizes(conn(ii,jj))
                error(['ERROR in connection statement ' num2str(ii) ': device number ' num2str(conn(ii,jj)) ' does not have ' num2str(conn(ii,jj+1)) ' ports'])
            end
        else
            error(['ERROR in connection statement ' num2str(ii) ': does not exist, device number is too big'])
        end
    end
end

% translate (device port) into a port in the big S matrix
connS=zeros(N,2);
for ii=1:N
    if conn(ii,1)==1
        connS(ii,1)=conn(ii,2);
    else
        connS(ii,1)=sum(sizes(1:conn(ii,1)-1))+conn(ii,2);
    end
    if conn(ii,3)==1
        connS(ii,2)=conn(ii,4);
    else
        connS(ii,2)=sum(sizes(1:conn(ii,3)-1))+conn(ii,4);
    end
end    

% check whether every port was only used once
if length(unique(connS(:)))~=2*N
    error('every port can only be used once in a connection. use T sections to connect multiple')
end

if sparsemode
    % SPARSE MODE, G is a sparse matrix
    i=zeros(2*length(connS),1);
    j=zeros(2*length(connS),1);
    for ii=1:length(connS)
        i(2*(ii-1)+1)=connS(ii,1);
        j(2*(ii-1)+1)=connS(ii,2);
        i(2*(ii-1)+2)=connS(ii,2);
        j(2*(ii-1)+2)=connS(ii,1);
    end
    G = sparse(i,j,ones(size(i)),sum(sizes),sum(sizes));
    
    if debug
        figure
        spy(G)
    end
else
    G = zeros(sum(sizes));
    % NORMAL (FULL MATRIX) MODE, G is just a matrix
    for ii=1:N
        G(connS(ii,1),connS(ii,2))=1;
        G(connS(ii,2),connS(ii,1))=1;
    end
    
    if debug
        figure
        spy(G);
    end
end

%% find the ports which are not used in the list

unconn = setdiff(1:sum(sizes),connS(:));

if debug
    disp('The unconnected ports are:')
    disp(unconn);
    disp('The connection vector connS is:')
    disp(connS);
end

%% Apply the monaco method

if debug
    disp('Applying Monaco Method;')
    figure('name','G matrix suplied to the monaco method');
    spy(G(sort(connS(:)),sort(connS(:))));
end

Sp = monacoMethod( S , G(sort(connS(:)),sort(connS(:))) , unconn );


end


