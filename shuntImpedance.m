function res = shuntImpedance(type,component,V,freq,Z0)
% SHUNTIMPEDANCE gives the two-port matrix of a shunt impedance
%
%   res = shuntImpedance(type,component,V,freq)
%   res = shuntImpedance(type,component,V,freq,Z0)
%
% Inputs
%   type        Char representing the two-port representation wanted. 
%               Options are: 'Z', 'Y', 'A', 'B', 'G', 'H', 'S' or 'T'
%   component   Char representing the wanted component type. 
%               Options are: 'R', 'C' or 'L'
%   V           is the value of the component.
%                   if 'R' is chosen, V is a resistance in Ohm.
%                   if 'L' is chosen, V is an inductance in Henry.
%                   if 'C' is chosen, V is a capacitance in Farad.
%   freq        1xN vector which contains the frequencies at which the
%               impedance should be returned. the frequencies are in Hertz.
%   Z0          is the characteristic impedance used when an S or T-matrix is asked.
%   
% Output
%   res         2 x 2 x F matrix representing the shunt impedance in the
%               asked representation at the different frequencies.
%
% See also SERIESIMPEDANCE, CONVERTREPRESENTATION
%
% Adam Cooman, ELEC VUB


if exist('freq','var')
    N = length(freq);
else
    N=1;
end

if all(cellfun(@isempty,regexp({'^Z$', '^Y$', '^A$', '^B$', '^G$', '^H$', '^S$' , '^T$'},type)))
    error('specified type is unknown. options are: ''Z'', ''Y'', ''A'', ''B'', ''G'', ''H'', ''S'' or ''T''');
end
    

if upper(component) == 'R'
    R = V;
    Z_matrix = R .* ones(2,2,N);
    
elseif upper(component) == 'C'
    if ~exist('freq','var')
        error('for reactive components, the frequency should be specified')
    end
    C = V;
    if N==1
        Z_matrix =  1/(1i*2*pi*freq*C) .* ones(2,2);
    else
        % preallocate
        Z_matrix = zeros(2,2,N);
        for ii=1:N
            Z_matrix(:,:,ii) = 1/(1i*2*pi*freq(ii)*C) .* ones(2,2);
        end
    end

elseif upper(component) == 'L'
    if ~exist('freq','var')
        error('for reactive components, the frequency should be specified')
    end
    L = V;
    if N==1
         Z_matrix = 1i*2*pi*freq*L .* ones(2,2);
    else
        % preallocate
        Z_matrix = zeros(2,2,N);
        for ii=1:N
            Z_matrix(:,:,ii) = 1i*2*pi*freq(ii)*L .* ones(2,2);
        end
    end
else
    error('component type unknown, possibilities are R, C or L')
end
    
if strcmp(type,'S')||strcmp(type,'T')
    if ~exist('Z0','var')
        error('if an S or T-matrix is to be returned, the characteristic impedance should be specified')
    end
    res = convertRepresentation(Z_matrix,'Z',type,Z0);
elseif strcmp(type,'Z')
    res = Z_matrix;
else
    res = convertRepresentation(Z_matrix,'Z',type);
end


end