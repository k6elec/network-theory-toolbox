%% Functions by Category
% Network Theory Toolbox
% Version 1.1 26-Aug-2013
%
%% Convert functions 
%   convertRepresentation - converts a matrix representation of a network into another.
%   unbal2bal             - transforms a multiport S matrix into a generalised mixed-mode S-matrix
% 
%% Connect functions
%   multiportConnection   - connects a series of S-matrices together to obtain the S-matrix of the total circuit
%   connectPorts          - connects two ports of an Y-matrix representation to present one port.
%   suppressPorts         - Leave ports open in a network
%   
%% Synthesis functions
%   seriesImpedance       - gives the two-port matrix of a series impedance
%   shuntImpedance        - gives the two-port matrix of a shunt impedance
%% Misc functions
%   makewaves             - this function makes wave spectra out of current and voltage spectra.
%   plot_S_matrix         - plots an S-matrix in a subplot.

