function plot_S_matrix(varargin)
% PLOT_S_MATRIX plots an S-matrix in a subplot.
%
%   plot_S_matrix(S)
%   plot_S_matrix(F,S)
%   plot_S_matrix(S,lineprops)
%   plot_S_matrix(F,S,lineprops)
%
% Inputs
%   S           N x N x F S-matrix
%   F           1 x F frequency vector
%   lineprops   line properties as you would provide them to a normal plot
%
% Output
% plot_S_matrix plots an s-matrix on an N x N subplot grid. All diagonal
% elements are plotted on a smith chart, while the off diagonal elements
% are plotted in db on a semilogx plot.
%
% Notes:
%
% See also SMITHCHART
%
% Version History:
% Adam Cooman, ELEC VUB

Nin =length(varargin);
if Nin==1
    % only an S-matrix is given, no frequencies
    S = varargin{1};
elseif Nin==2
    if ~ischar(varargin{2})
        % ' no line properties are specified '
        S = varargin{2};
        F = varargin{1};
    else
        % an S matrix and line properties are specified
        S = varargin{1};
        p = varargin{2};
    end
elseif Nin==3
    % frequency, s-matrix and line properties are specified
    F = varargin{1};
    S = varargin{2};
    p = varargin{3};
end

% check the povided inputs
% S should be an NxNxF matrix

    sizeS = size(S);
    % first check the size of S
    if (length(sizeS)>3)||(length(sizeS)==1)
        error('S should be a 2 or 3-dimensional matrix')
    end
    if length(sizeS)==2
        sizeS(3)=1;
    end
    % make sure S is a square matrix
    N = sizeS(1);
    if N~=sizeS(2)
        error('S should be square')
    end


% F should be an Fx1 vector
if ~exist('F','var')
    F=1:sizeS(3);
else
    if isvector(F)
        if length(F)~=sizeS(3)
            error('F should have the same length as S(1,1,:)')
        end
    else
        error('F should be a vector')
    end
end

if ~exist('p','var')
    p='-';
end



%% Do the actual plotting (finally)

% split up the specified line properties into a marker and a line parameter
linestyles={'-','--',':','-.'};
line=getstring(p,linestyles,'none');

% do the same with the colours
colours={'b','r','g','k','y','m','c','w'};
color=getstring(p,colours,'b');

% all that remains is the symbol
marker=setdiff(p,[line color]);

for ii=1:N
    for jj=1:N
        subplot(N,N,(ii-1)*N+jj)
        if ii==jj
            lin = smithchart(squeeze(S(ii,jj,:)));
            % first the linestyle
            set(lin,'LineStyle',line);
            % then the marker
            if ~isempty(marker)
                set(lin,'Marker',marker);
            end
            % finally the colour
            set(lin,'MarkerEdgeColor',color)
        else
            semilogx(F,db(squeeze(S(ii,jj,:))),p)
            grid on
        end
        title(['S' num2str(ii) num2str(jj)])
    end
end


    function res=getstring(string,possibilities,default)
        for kk=length(possibilities):-1:1
            findres = strfind(string,possibilities{kk});
            if ~isempty(findres)
                res = possibilities{kk};
                break
            end
        end
        if ~exist('res','var')
            res=default;
        end
        
        
    end

end