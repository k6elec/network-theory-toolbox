function [A,B] = makewaves_v2(V,I,mixedPorts,Z0,type)
% MAKEWAVES_v2 converts voltages and currents to waves following the technique from Ferrero
%
%   W = makewaves_v2(V,I,mixedPorts,Z0,type)
%   [A,B] = makewaves_v2(V,I,mixedPorts,Z0,type)
%
% Inputs: 
%  V            N x F matrix which contains the voltages measured at F frequencies at the N ports.
%  I            N x F matrix which contains the currents measured at F frequencies at the N ports.
%  mixedPorts   2xM matrix which contains the ports that should be combined
%               in a differential port.
%  Z0           characteristic impedance of every port. you can pass a
%               scalar or a vector. If Z0 is a scalar, single ended ports
%               will have Z0 as characteristic impedance, differential
%               ports will have 2*Z0 and common-mode ports will have Z0/2.
%  type         wave type used. Either 'pseudo' or 'power'
%
% Output:
%  A,B          both N x F matrices which contain the A and B waves.
%
% Adam Cooman, ELEC VUB


%% check the inputs thouroughly
[N,F] = size(V);
[N2,F2] = size(I);
if (N2~=N)||(F2~=F)
    error('V and I should have the same size')
end
clear N2 F2

if ~exist('mixedPorts','var')
    % if the mixedports are not provided, all ports are assumed single-ended
else
    % if the mixedports are provided (and not empty) the matrix is thouroughly checked
    if isempty(mixedPorts)
        if mod(N,2)~=0
            error('the amount of ports should be even.');
        end
        p=N/2;
    else
        [p,M]=size(mixedPorts);
        % do a lot of checking on the mixedPorts vector
        mixedPorts = checkMixedPorts(mixedPorts,N,M,p);
    end
end

if ~exist('Z0','var')
    Z0 = 50*ones(1,N);
    for ii=1:M
        Z0(mixedPorts(1,ii))=100;
        Z0(mixedPorts(2,ii))=25;
    end
else
    if isscalar(Z0)
        Zref = Z0;
        Z0 = Zref*ones(1,N);
        for ii=1:M
            Z0(mixedPorts(1,ii))=2*Zref;
            Z0(mixedPorts(2,ii))=Zref/2;
        end
    else
        if isvector(Z0)
            if length(Z0)~=N
                error('Z0 should have length N');
            end
        else
            error('Z0 should be a scalar or a vector length of length 2*N');
        end
    end
end


%% do the actual calculations

% build the R matrix
R = zeros(2*N,F);
R(1:2:end)=V;
R(2:2:end)=I;

% build the transformation matrix to go to differential and common-mode voltages and currents
T = eye(2*N);
Tdiff = [1 0 -1 0;0 0.5 0 -0.5; 0.5 0 0.5 0;0 1 0 1];
for ii=1:M
    j = mixedPorts(1,ii);
    k = mixedPorts(2,ii);
    range = [2*(j-1)+1 2*(j-1)+2 2*(k-1)+1 2*(k-1)+2]; 
    T(range,range) = Tdiff;
end

% build the transformation matrix to go from voltages and currents to waves
switch lower(type)
    case 'pseudo'
        wavetransform = @pseudowave;
    case 'power'
        wavetransform = @powerwave;
    otherwise
        error('wave type unknown')
end
M = zeros(N);
for ii=1:N
    range = [2*(ii-1)+1 2*(ii-1)+2];
    M(range,range)=wavetransform(portimp(ii));
end

% build the complete transformation matrix by multiplying the M and T matrix
X = M*T;

% apply the transformation at all the frequencies
W = zeros(size(R));
for ff=1:F
    W(:,ff) = X*R(:,ff);
end

% get the 
A = W(1:2:end,:);
B = W(2:2:end,:);


end

% wave transformation functions
function res = powerwave(Zj)
res = 1/(2*sqrt(real(Zj)))*[1 Zj;1 -Zj];
end

function res = pseudowave(Zj)
res = sqrt(real(Zj))/(2*abs(Zj))*[1 Zj;1 -Zj];
end

function mixedPorts = checkMixedPorts(mixedPorts,N,M,p)
% checks the mixedPorts for anomalies.

% check whether it is a 2xM matrix, if it's a Mx2 matrix, permute, warn and continue
if M~=2
    if p==2
        mixedPorts = mixedPorts';
        warning('mixedPorts has been permuted. it should be a 2xM matrix');
        p=M;
        M=2;
    else
        error('the MixedPorts matrix should be 2xM, you can only combine 2 ports at a time.')
    end
end
if p>N/2
    error('the amount of ports to combine cannot exceed the total amount of ports');
end
if any(find(mixedPorts>N))
    error('all elements in mixedPorts should be smaller than the size of the S-matrix');
end
if any(find(mixedPorts<1))
    error('all elements in mixedPorts should be larger than 1');
end
if length(unique(mixedPorts(:)))~=2*p
    error('every port can only be in mixedPorts once.');
end
if ~isempty(setdiff(mixedPorts(:),round(mixedPorts(:))))
    error('all ports should have integer numbers.');
end

end
