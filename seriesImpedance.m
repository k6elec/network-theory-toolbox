function res = seriesImpedance_adapted(type,component,V,freq,Z0)
% SERIESIMPEDANCE gives the two-port matrix of a series impedance
%
%   res = seriesImpedance(type,component,V,freq)
%   res = seriesImpedance(type,component,V,freq,Z0)
%
% Inputs
%   type        Char representing the two-port representation wanted. 
%               Options are: 'Z', 'Y', 'A', 'B', 'G', 'H', 'S' or 'T'
%   component   Char representing the wanted component type. 
%               Options are: 'R', 'C' or 'L'
%   V           is the value of the component.
%                   if 'R' is chosen, V is a resistance in Ohm.
%                   if 'L' is chosen, V is an inductance in Henry.
%                   if 'C' is chosen, V is a capacitance in Farad.
%   freq        1xN vector which contains the frequencies at which the
%               impedance should be returned. the frequencies are in Hertz.
%   Z0          is the characteristic impedance used when an S or T-matrix is asked.
%   
% Output
%   res         2 x 2 x F matrix representing the series impedance in the
%               asked representation at the different frequencies.
%
% See also SHUNTIMPEDANCE, CONVERTREPRESENTATION
%
% Adam Cooman, ELEC VUB

if exist('freq','var')
    N = length(freq);
else
    N=1;
end


if upper(component) == 'R'
    R = V;
    Y_temp = zeros(2,2,N);
    Y_temp(1,1,:)=ones(1,N);
    Y_temp(2,1,:)=-ones(1,N);
    Y_temp(2,2,:)=ones(1,N);
    Y_temp(1,2,:)=-ones(1,N);
    Y_matrix = 1/R .* Y_temp;
    
elseif upper(component) == 'C'
    if ~exist('freq','var')
        error('for reactive components, the frequency should be specified')
    end
    C=V;
    
    if N==1
        Y_matrix = (1i*2*pi*freq*C) .* [1 -1;-1 1];
    else
        % preallocate
        Y_matrix = zeros(2,2,N);
        for ii=1:N
            Y_matrix(:,:,ii) = (1i*2*pi*freq(ii)*C) .* [1 -1;-1 1];
        end
    end

elseif upper(component) == 'L'
    if ~exist('freq','var')
        error('for reactive components, the frequency should be specified')
    end
    L = V;
    
    if N==1
        Y_matrix = (1i*2*pi*freq*L) ^(-1) *[1 -1;-1 1];
    else
        % preallocate
        Y_matrix = zeros(2,2,N);
        for ii=1:N
            Y_matrix(:,:,ii) = (1i*2*pi*freq(ii)*L)^-1 * [1 -1;-1 1];
        end
    end
else
    error('component type unknown, possibilities are R, C or L')
end

    
if strcmp(type,'S')
    if ~exist('Z0','var')
        error('if an S-matrix is to be returned, the characteristic impedance should be specified')
    end
    res = convertRepresentation(Y_matrix,'Y',type,Z0);
elseif strcmp(type,'Y')
    res = Y_matrix;
else
    res = convertRepresentation(Y_matrix,'Y',type);
end


end