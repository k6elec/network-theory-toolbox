function D = dembedPackage(IN,varargin)
% this function de-embeds the package of a component by calculating a 
% S-matrix that negates the S-matrix of the package when used in cascade.
%
%   D = makewaves(fileIN,fileOUT)
%
% With:
%   IN   Is the name of the output SNP-file, ready to use in ADS. 
%                                   OR
%      Is the NxNxF S-matrix of the package with N being an even number 
%   fileOUT   Is the name of the output SNP-file, ready to use in ADS.
%
% Output:
%   D   The S-matrix of the compensation component
%
%   ELEC VUB; Piet Bronders

%% parse the input
p = inputParser();
% Input (Ethier a filename or a NxNxF array)
p.addRequired('IN',@(x) ischar(x) || checkS_Matrix(x));
% Frequencies of the S-matrix.
p.addParamValue('freq',[],@(x) isnumeric(x));
% Output filename.
p.addParamValue('fileOUT',[],@(x) ischar(x));

p.StructExpand = true;
p.parse(IN,varargin{:});
args = p.Results();

%% pre-processing of given parameters
if ~isempty(args.fileOUT)
    % remove ext from fileout
    [locOUT,args.fileOUT,~]=fileparts(args.fileOUT);
    args.fileOUT = fullfile(locOUT,args.fileOUT);
end

% is IN a string?
if ischar(args.IN)
    % split the provided filename into bits
    [loc,name,~]=fileparts(args.IN);
    if isempty(loc)
        loc='.';
    end

    % check the local directory
    res = dir(loc);
    names={res.name};

    % look for fileIN.sXp
    regres = regexp(names,['^' name '\.s[0-9]+p$'],'once');
    regres = find(cellfun(@(x) ~isempty(x),regres));

    if length(regres)~=1
        error('file not found or multiple files found')
    else
        args.IN = fullfile(loc,names{regres});
    end

    S = sparameters(args.IN);
else
    if isempty(args.freq)
        warning('No frequency vector was supplied thus the function chose to use [1:F*1].');
        args.freq=1:size(args.IN,3);
    elseif length(args.freq)~=size(args.IN,3)
        warning('Dimension of the supplied frequency vector is INCORRECT. Using the interval [1:F*1] instead');
        args.freq=1:size(args.IN,3);
    end
    S=sparameters(args.IN,args.freq);
end

N=size(S.Parameters,1);
if rem(N,2)
    error('The dimension of the S-matrix should be even!');
end

%% Calculate the inverse of the S-matrix.
        D=S.Parameters;
        for ii=1:size(S.Parameters,3)
            D(:,:,ii)=eye(N,N)/S.Parameters(:,:,ii);
        end

%% permute the first two row with the last & permute the first two columns 
% with the last.
        D(:,[1:N/2 (1+N/2):N],:) = D(:,[(1+N/2):N 1:N/2],:);
        D([1:N/2 (1+N/2):N],:,:) = D([(1+N/2):N 1:N/2],:,:);
        
hs=sparameters(D,S.Frequencies);

if ~isempty(args.fileOUT)
rfwrite(hs, [ args.fileOUT '.s' num2str(N) 'p']);
end

end

%% function to check the spectrum passed to the function
function res = checkS_Matrix(s)

if size(size(s))==[1 3]
    if size(s,1)==size(s,2)
        res=1;
    else
        error('The supplied S-matrix is not a square matrix.')
        res=0;
    end
else
    error('The supplied S-matrix contains more than 3 dimensions.')
    res=0;
end

end