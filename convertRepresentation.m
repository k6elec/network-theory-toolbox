function res = convertRepresentation(varargin)
% convertRepresentation converts a matrix representation of a network into another
% 
%      res = convertRepresentation(IN,typeIn,typeOut);
%      res = convertRepresentation(IN,typeIn,typeOut,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - IN      check: @isnumeric
%      Network matrix in a certain representation. This is a NxNxF
%      matrix.
% - typeIn      check: @(x) any(strcmpi(x,{'Z','Y','S','T','A','G','H'}))
%      network representation in which the input matrix is represented
% - typeOut      check: @(x) any(strcmpi(x,{'Z','Y','S','T','A','G','H'}))
%      wanted network representation for the output
% 
% Parameter/Value pairs:
% - 'Z0'     default: 50  check: @isscalar
%      characteristic impedance. For now, it is only possible to have
%      a single  characteristic impedance for all ports, no weird ports
%      with
% - 'wavetype'     default: 'POWER'  check: @(x) any(strcmpi(x,{'power','pseudo'}))
%      type of waves used in the representations. Depending on the
%      choice,  either Kurokawa's power waves or Williams' pseudo waves
%      are used. For a  real positive characteristic impedance, this
%      all makes no difference.
% 
% Outputs:
% - res      Type: NxNxF matrix
%      network matrix in the chosen representation type
% 
%   EXAMPLE:
%        % convert S-paramters to Y paramters
%        Y = convertRepresentation(S,'S','Y');
% 
% Adam Cooman, ELEC, VUB
% 
% 
% This documentation was generated with the generateFunctionHelp function at 12-Jul-2016

p = inputParser();
% Network matrix in a certain representation. This is a NxNxF matrix.
p.addRequired('IN',@isnumeric);
% network representation in which the input matrix is represented
p.addRequired('typeIn',@(x) any(strcmpi(x,{'Z','Y','S','T','A','G','H'})));
% wanted network representation for the output
p.addRequired('typeOut',@(x) any(strcmpi(x,{'Z','Y','S','T','A','G','H'})));
% characteristic impedance. For now, it is only possible to have a single
% characteristic impedance for all ports, no weird ports with 
p.addParameter('Z0',50,@isscalar);
% type of waves used in the representations. Depending on the choice,
% either Kurokawa's power waves or Williams' pseudo waves are used. For a
% real positive characteristic impedance, this all makes no difference.
p.addParameter('wavetype','POWER',@(x) any(strcmpi(x,{'power','pseudo'})));
p.parse(varargin{:});
args = p.Results;
clear varargin

% perform checks on A
theSize=size(args.IN);
switch length(theSize)
    case 2
        N = theSize(1);
        N2 = theSize(2);
        F = 1;
    case 3
        N = theSize(1);
        N2 = theSize(2);
        F = theSize(3);
    otherwise
        error('The input should have maximum 3 dimensions: NxNxF where N is the amount of ports and F is the amount of frequencies')
end

% check whether the input is a square matrix
if N~=N2
    error('network matrices should be square')
end
clear N2

% when more than 2 ports are present or if a single port is present, ensure that the type is only Y Z or S
if (N>2)||(N==1)
    possibletypes={'Z','Y','S'};
    if (~ismember(upper(args.typeIn),possibletypes))||(~ismember(upper(args.typeOut),possibletypes))
        error('the specified type is not among the known types')
    end
end

% check Z0 and generate the vector
if args.Z0==0
    error('The characteristic impedance cannnot be equal to zero')
end
Z = args.Z0;
Y = 1./args.Z0;

% set the wave factor k
switch upper(args.wavetype)
    case 'POWER'
        k = 1/(2*sqrt(real(Z)));
    case 'PSEUDO'
        k = sqrt(real(Z))/(2*sqrt(Z*Z'));
end
    
% build the P matrix that describes the transform
switch upper(args.typeIn)
    case 'Y'
        switch upper(args.typeOut)
            case 'Y'
                P = eye(2*N);
            case 'Z'
                P = [zeros(N) eye(N);eye(N) zeros(N)];
            case 'G'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'H'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'A'
                P = [0 0 1 0;1 0 0 0;0 0 0 1;0 -1 0 0];
            case 'B'
                error('TODO');
            case 'S'
                P = k*[-Z*eye(N) eye(N);Z*eye(N) eye(N)];
            case 'T'
                P = k*[0 -Z 0 1;0 Z 0 1; Z 0 1 0;-Z 0 1 0];
        end
    case 'Z'
        switch upper(args.typeOut)
            case 'Y'
                P = [zeros(N) eye(N);eye(N) zeros(N)];
            case 'Z'
                P = eye(2*N);
            case 'G'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'H'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'A'
                P = [1 0 0 0;0 0 1 0;0 1 0 0;0 0 0 -1];
            case 'B'
                error('TODO')
            case 'S'
                P = k*[eye(N) -Z*eye(N);eye(N) Z*eye(N)];
            case 'T'
                P = k*[0 1 0 -Z;0 1 0 Z;1 0 Z 0;1 0 -Z 0];
        end
    case 'G'
        switch upper(args.typeOut)
            case 'Y'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'Z'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'G'
                P = eye(2*N);
            case 'H'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'A'
                P = [0 0 1 0;1 0 0 0;0 1 0 0;0 0 0 -1];
            case 'B'
                error('TODO')
            case 'S'
                P = [-Z 0 1 0;0 1 0 -Z;Z 0 1 0;0 1 0 Z];
            case 'T'
                P = [0 1 0 -Z;0 1 0 Z;Z 0 1 0;-Z 0 1 0];
        end
    case 'H'
        switch upper(args.typeOut)
            case 'Y'
                P = [0 0 1 0;0 1 0 0;1 0 0 0;0 0 0 1];
            case 'Z'
                P = [1 0 0 0;0 0 0 1;0 0 1 0;0 1 0 0];
            case 'G'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'H'
                P = eye(2*N);
            case 'A'
                P = [1 0 0 0;0 0 1 0;0 0 0 1;0 -1 0 0];
            case 'B'
                error('TODO')
            case 'S'
                P = [1 0 -Z 0;0 -Z 0 1;1 0 Z 0;0 Z 0 1];
            case 'T'
                P = [0 -Z 0 1;0 Z 0 1;1 0 Z 0;1 0 -Z 0];
        end
    case 'A'
        switch upper(args.typeOut)
            case 'Y'
                P = [0 1 0 0;0 0 0 -1;1 0 0 0;0 0 1 0];
            case 'Z'
                P = [1 0 0 0;0 0 1 0;0 1 0 0;0 0 0 -1];
            case 'G'
                P = [0 1 0 0;0 0 1 0;1 0 0 0;0 0 0 -1];
            case 'H'
                P = [1 0 0 0;0 0 0 -1;0 1 0 0;0 0 1 0];
            case 'A'
                P = eye(2*N);
            case 'B'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'S'
                P = [1 -Z 0 0;0 0 1 Z;1 Z 0 0;0 0 1 -Z];
            case 'T'
                P = [0 0 1 Z;0 0 1 -Z;1 Z 0 0;1 -Z 0 0];
        end
    case 'B'
        switch upper(args.typeOut)
            case 'Y'
                error('TODO');
            case 'Z'
                error('TODO');
            case 'G'
                error('TODO');
            case 'H'
                error('TODO');
            case 'A'
                P = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
            case 'B'
                P = eye(2*N);
            case 'S'
                error('TODO');
            case 'T'
                error('TODO');
        end
    case 'S'
        switch upper(args.typeOut)
            case 'Y'
                P = 1/2/k*[-Y*eye(N) Y*eye(N);eye(N) eye(N)];
            case 'Z'
                P = 1/2/k*[eye(N) eye(N);-Y*eye(N) Y*eye(N)];
            case 'G'
                P = 1/2/k*[-Y 0 Y 0;0 1 0 1;1 0 1 0;0 -Y 0 Y];
            case 'H'
                P = 1/2/k*[1 0 1 0;0 -Y 0 Y;-Y 0 Y 0;0 1 0 1];
            case 'A'
                P = 1/2/k*[1 0 1 0;-Y 0 Y 0;0 1 0 1;0 Y 0 -Y];
            case 'B'
                error('TODO');
            case 'S'
                P = eye(2*N);
            case 'T'
                P = [0 1 0 0;0 0 0 1;0 0 1 0;1 0 0 0];
        end
    case 'T'
        switch upper(args.typeOut)
            case 'Y'
                P = 1/2/k*[0 0 Y -Y;-Y Y 0 0;0 0 1 1;1 1 0 0];
            case 'Z'
                P = 1/2/k*[0 0 1 1;1 1 0 0;0 0 Y -Y;-Y Y 0 0];
            case 'G'
                P = 1/2/k*[0 0 Y -Y;1 1 0 0;0 0 1 1;-Y Y 0 0];
            case 'H'
                P = 1/2/k*[0 0 1 1;-Y Y 0 0;0 0 Y -Y;1 1 0 0];
            case 'A'
                P = 1/2/k*[0 0 1 1;0 0 Y -Y;1 1 0 0;Y -Y 0 0];
            case 'B'
                error('TODO')
            case 'S'
                P = [0 0 0 1;1 0 0 0;0 0 1 0;0 1 0 0];
            case 'T'
                P = eye(2*N);
        end
end


% now perform the actual conversion
P11 = P(1:N,1:N);
P12 = P(1:N,N+1:2*N);
P21 = P(N+1:2*N,1:N);
P22 = P(N+1:2*N,N+1:2*N);
res = zeros(N,N,F);
for ff=1:F
    res(:,:,ff) = (P11*args.IN(:,:,ff)+P12)/(P21*args.IN(:,:,ff)+P22);
end


end

% @generateFunctionHelp
% @output1 network matrix in the chosen representation type
% @outputType1 NxNxF matrix
% @author Adam Cooman
% @institution ELEC, VUB
% @tagline converts a matrix representation of a network into another
% @example EXAMPLE:
% @example      % convert S-paramters to Y paramters
% @example      Y = convertRepresentation(S,'S','Y');

%% generateFunctionHelp: old help, backed up at 12-Jul-2016. leave this at the end of the function
% convertRepresentation converts a matrix representation of a network into another
% 
%      res = convertRepresentation(IN,typeIn,typeOut);
%      res = convertRepresentation(IN,typeIn,typeOut,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - IN      check: @isnumeric
%      Network matrix in a certain representation. This is a NxNxF
%      matrix.
% - typeIn      check: @(x) any(strcmpi(x,{'Z','Y','S','T','A','G','H'}))
%      network representation in which the input matrix is represented
% - typeOut      check: @(x) any(strcmpi(x,{'Z','Y','S','T','A','G','H'}))
%      wanted network representation for the output
% 
% Parameter/Value pairs:
% - 'Z0'     default: 50  check: @isscalar
%      characteristic impedance. For now, it is only possible to have
%      a single  characteristic impedance for all ports, no weird ports
%      with
% - 'wavetype'     default: 'POWER'  check: @(x) any(strcmpi(x,{'power','pseudo'}))
%      type of waves used in the representations. Depending on the
%      choice,  either Kurokawa's power waves or Williams' pseudo waves
%      are used. For a  real positive characteristic impedance, this
%      all makes no difference.
% 
% Outputs:
% - res      Type: NxNxF matrix
%      network matrix in the chosen representation type
% 
%   EXAMPLE:
%        % convert S-paramters to Y paramters
%        Y = convertRepresentation(S,'S','Y');
% 
% Adam Cooman
% 
% 
% This documentation was generated with the generateFunctionHelp function at 12-Jul-2016
