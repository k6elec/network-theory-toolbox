% this script tests the conversion functions by going from one representation to another and back
clc
close all
clear variables

% these are the different representations that will be tested
rep = {'Z','Y','G','H','A','S','T'};

pass = 'S';

% N is the amount of ports in the circuit;
N = 2;

for ii=1:length(rep)
    for jj=1:length(rep)
            % the system is a random matrix
            sys = randn([N N 100])+1i*randn([N N 100]);
            from = rep{ii};
            to = rep{jj};
            % convert the representation from a certain type to another,
            % possibly passing a third representation
            if ~isempty(pass)
                temp = convertRepresentation(sys,from,pass);
                temp = convertRepresentation(temp,pass,to);
                syst = convertRepresentation(temp,to,from);
            else
                temp = convertRepresentation(sys,from,to);
                syst = convertRepresentation(temp,to,from);
            end
            
            % the result is the sum of the errors
            result = sum(sum(sum(abs(sys-syst),3),2),1);
            if ~isempty(pass)
                disp([from ' over ' pass ' to ' to ' and back to ' from ': ' num2str(result)]);
            else
                disp([from ' to ' to ' and back: ' num2str(result)]);
            end
            
            % if the result is too big, show some stuff of the test
            if result>1e-9
                if ~isempty(pass)
                    warning(['WARNING: error in path ' from '->' pass '->' to '->' from])
                else
                    warning(['WARNING: error in path ' from '->' to '->' from]);
                end
%                 figure('name',['errors in ' funct])
%                 res = abs(sys-syst);
%                 plot(squeeze(res(1,1,:)),'b')
%                 hold on
%                 plot(squeeze(res(1,2,:)),'r')
%                 plot(squeeze(res(2,1,:)),'k')
%                 plot(squeeze(res(2,2,:)),'g')
%                 legend('11','12','21','22')
            end
    end
end