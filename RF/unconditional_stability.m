function [K,D]=unconditional_stability(S,display)
% check wheter the S-matrix is unconditionally stable
%
% [K,D]=unconditional_stability(S)
%
% This function calculates K and Delta to check whether the S-matrix is
% unconditionally stable.
% 
% I use the formula's from gonzales for K and Delta
%
% Adam Cooman, ELEC VUB

if ~exist('display','var')
    display=1;
end

D = det(S);

K = (1-abs(S(1,1))^2 - abs(S(2,2))^2 + abs(D)^2 )/(2*abs(S(1,2)*S(2,1)));

if display
    if K<1
        disp('S matrix is not unconditionally stable');
    end
end
