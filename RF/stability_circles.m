function [Cl,rl,Cs,rs] = stability_circles(S)
% this function give the center and radius of the stability circles.
%
% [Cl,rl,Cs,rs] = stability_circles(S)
%
% The formula's from Gonzalez are used
%
% Adam Cooman, ELEC VUB
d = det(S);

denom = abs(S(2,2))^2 - abs(d)^2;
rl = abs(S(1,2)*S(2,1)/denom);
Cl = conj(S(2,2)-d*conj(S(1,1)))/denom;

denom = abs(S(1,1))^2 - abs(d)^2;
rs = abs(S(1,2)*S(2,1)/denom);
Cs = conj(S(1,1)-d*conj(S(2,2)))/denom;

end