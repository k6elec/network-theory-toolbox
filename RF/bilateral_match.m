function [Gin,Gout] = bilateral_match(S)
% this function gives the input and output reflection coefficient such that the S-matrix is fully matched at in-and output
%
% [Gin,Gout] = bilateral_match(S)
%
% We use the formula's found in Gonzalez
% We assume an unconditionnaly stable S-matrix and use the solution with
% the minus sign as is explained in Gonzalez
%
% Adam Cooman, ELEC VUB


[K,D]=unconditional_stability(S,0);



B1 = 1 + abs(S(1,1))^2 - abs(S(2,2))^2 - abs(D)^2;
B2 = 1 + abs(S(2,2))^2 - abs(S(1,1))^2 - abs(D)^2;

C1 = S(1,1) - D*conj(S(2,2));
C2 = S(2,2) - D*conj(S(1,1));

if K<1
    warning('circuit is not unconditionally stable, result cannot be trusted');
    
    % TODO, if the circuit is not unconditionally stable, the maximum stable
    % power gain should be used to obtain a result. This can be quite hard to
    % implement though.
    Gmsg = abs(S(2,1))/abs(S(1,2));
    % default backoff is 25%
    backoff = 0.25;
    
    Gp = Gmsg*(1-backoff);
    
    % draw the constant gain circle for the backed off power gain
    
    % find the point on that circle which is the furthest away from the
    % stability circle
    
    error('K<1 has not been implemented yet')
    
else
    
    if abs(D)>1
        warning('stability problem: K>1 but D>1, designing for Gt,min');
        % design for Gt,min. Using the solution with the + sign should
        % result in reflection factors smaller than 1.
        Gin  = (B1 + sqrt(B1^2-4*abs(C1)^2))/(2*C1);
        Gout = (B2 + sqrt(B2^2-4*abs(C2)^2))/(2*C2);
    else
        % normal uncondditionally stable case
        Gin  = (B1 - sqrt(B1^2-4*abs(C1)^2))/(2*C1);
        Gout = (B2 - sqrt(B2^2-4*abs(C2)^2))/(2*C2);
    end
end



end