function [res,t,Cvecres] = rijen_permutatie(M,selectie,last,CvecM)
% permuteren van gekozen rijen (geselecteerd door indices)
% Danku aan Jan om de functie te schrijven
%
% INPUT
% M         de matrix die het linear stelsel beschrijft
% selectie  de vergelijkingen die naar voor of achter verhuisd moeten worden
% last      boolean die aangeeft waar de vergelijkingen moeten komen, als
%           last=1, dan worden ze achteraan geplaatst, als last=0 dan worden ze
%           vooraan geplaatst. default=1;
% CvecM     covariantie matrix van vec(M)
%
% OUTPUT
% res       de nieuwe matrix die het stelsel beschrijft
% t         de transformatie matrix die gebruikt werd
% Cvecres   nieuwe covariantie matrix van vec(res)


% maak indices voor de rijen van de parameter vector b
if ~exist('last','var')
    last = 1;
end

if iscolumn(selectie)
    selectie = selectie';
end

[N,~,F] = size(M);

% build the transformation matrix
indices = 1:N;
indices(selectie) = [];
if last
    indices = [indices,selectie];
else
    indices = [selectie,indices];
end
t = eye(N);
t = t(indices,:);

% left and right multiply with the transformation matrix
res=zeros(size(M));
for ff=1:F
    res(:,:,ff) = t*M(:,:,ff)*t';
end

% if the CvecM is passed, also change its order
if exist('CvecM','var')
    T = kron(t,t);
    Cvecres = zeros(size(CvecM));
    for ff=1:F
        Cvecres(:,:,ff) = T*CvecM(:,:,ff)*T';
    end
else
    Cvecres = [];
end

end


