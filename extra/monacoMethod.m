function Sp = monacoMethod(S,G,ports)
% method described in the paper of Monaco1974 to determine the global 
% S matrix of a lot of interconnected smaller S-matrices.
%
%   S       is the S-matrix of all the parts in a block diagonal matrix
%   G       is the connection matrix of size = size(S) - length(ports)
%   ports   contains the ports which are left after the procedure
%
%   Sp      

N = length(ports);
[M,~,F] = size(S);
sparsemode = iscell(S);

% preallocate the result matrix
Sp = zeros(N,N,F);

% put the wanted ports first
if sparsemode
    parfor ff=1:F
        fprintf(1,'calculating frequency point:%d',ff);
        Srp = sparse_rijen_permutatie(S{ff},ports);
        Spp = Srp(1:N,1:N);
        Spc = Srp(1:N,N+1:M);
        Scp = Srp(N+1:M,1:N);
        Scc = Srp(N+1:M,N+1:M);
        W = G - Scc;
        Sp(:,:,ff) = Spp + (Spc * pinv(full(W)) * Scp);
    end
else
    for ff=1:F
        S(:,:,ff) = rijen_permutatie(S(:,:,ff),ports,0);
    end
    % split S into parts
    Spp = S(1:N,1:N,:);
    Spc = S(1:N,N+1:M,:);
    Scp = S(N+1:M,1:N,:);
    Scc = S(N+1:M,N+1:M,:);
    W=repmat(G,[1 1 F])-Scc;
    for ff=1:F
        Sp(:,:,ff) = Spp(:,:,ff) + Spc(:,:,ff) * pinv(W(:,:,ff)) * Scp(:,:,ff);
    end
end

end