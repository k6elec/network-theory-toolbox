function res = connectZmatrices(Z1 , Z2 , p1 , p2 )
% CONNECTZMATRICES connects an M-port Z-matrix with an N-port Z-matrix.
%
%       Z = connectZmatrices(Z1 , Z2 , p1 , p2 )      
%
% Inputs
%   Z1      N x N Z-matrix describing the first system
%   Z2      M x M Z-matrix describing the second system
%   p1      is the vector of ports to be connected of Z1. length(p1)<=N
%   p2      is the vector of ports to be connected of Z2. length(p2)<=M
%           p1 and p2 should have the same length
%           p1 can only contain integers between 1 and M
%           p2 can only contain integers between 1 and N
% Output
%   Z       L x L matrix of the connected system
%

[Z1,F1] = checkYparamInput(Z1);
[Z1,F2] = checkYparamInput(Z1);

if F1~=F2
    error('both matrices should be specified at the same frequencies');
else
    F=F1;
end

% get the needed sizes
[M,~,~] = size(Z1);
[N,~,~] = size(Z2);
c = length(p1);
p = M - c;
q = N - c;

res = zeros(p+q,p+q,F);
for ii=1:F

% Sort the matrices such that the ports to be connected are put last.
Z1t = rijen_permutatie(Z1(:,:,ii),p1);
Z2t = rijen_permutatie(Z2(:,:,ii),p2);

% split both S-matrices into parts
[Z1pp,Z1cc,Z1cp,Z1pc] = split(Z1t,p);
[Z2qq,Z2cc,Z2cq,Z2qc] = split(Z2t,q);

% apply (2.56b) to the parts of the matrices to obtain the result
Zcc = (Z1cc + Z2cc)^-1;
res(:,:,ii) = [Z1pp zeros(p,q) ; zeros(q,p) Z2qq] + ...
        [ Z1pc zeros(p,c); zeros(q,c) Z2qc] * ...
        [-Zcc Zcc ; Zcc -Zcc ]  * ...
        [Z1cp zeros(c,q) ; zeros(c,p) Z2cq];
    
end

%% function to split the sorted S-matrix into parts
    function [pp,cc,cp,pc]=split(S,par)
        pp = S(1:par,1:par);
        cc = S((par+1):end,(par+1):end);
        cp = S((par+1):end,1:par);
        pc = S(1:par,(par+1):end);
    end




end