classdef NetworkMatrix
    properties(Hidden)
        possibleTypes = {'Y','Z','A','B','S','T','G','H'};
        N % number of ports in the system
        F % number of frequencies in the system
    end
    
    properties%(SetAccess = private)
        type        % character indicating the type. it should be one of the possibleTypes
        matrix      % network matrix N x N x F
        Freq        % frequency vector 1 x F
        Z0          % diagonal matrix which contains the characteristic impedance of every port.
    end
    
    % TODO: check whether the variables in the hidden properties should be addressed as NM.N or just as N
    % expand mtimes and backslash to make sure that it can handle with multiplication with Nx1(xF) vectors
    % and NxN(xF) matrices on both left and right hand side.
    % do you need to call a function as NM = funct(NM,params) or can you just have NM = funct(params) if you
    % want to use NM.funct(param) as a way of calling the function.
    
    methods
        %% constructor
        function NM = NetworkMatrix(type,freq,data,Z0,fast)
            % if anything is passed in fast, no checks are performed on the
            % input data. this mode should be used internally only
            if exist('fast','var')
                NM.type = type;
                NM.Freq = freq;
                NM.F = length(freq);
                NM.matrix = data;
                NM.N = size(data,1);
                if iswaves(NM)
                    NM.Z0 = Z0;
                end
            else
                % check everything thouroughly and assign it to the proper
                % field. get the type first;
                if ischar(type)
                    if any(strcmp(type,NM.possibleTypes))
                        NM.type = type;
                    else
                        error('specified type not supported')
                    end
                else
                    error('type passed should be a char or a string')
                end
                
                % set the frequencies
                if isvector(freq)
                    NM.Freq = freq;
                    NM.F = length(freq);
                else
                    error('specified frequencies should be put in a vector')
                end
                
                % if working with S or T matrices, characteristic impedances are needed
                if iswaves(NM)
                    if exist('Z0','var')
                        NM.Z0 = Z0;
                    else
                        error('S and T matrices need characteristic impedance')
                    end
                end
                
                % if data is passed, put it in the data field
                if exist('data','var')
                    theSize = size(data);
                    if theSize(1)==theSize(2)
                        NM.N = theSize(1);
                        if length(theSize)==3
                            if theSize(3)==NM.F
                                NM.matrix = data;
                            else
                                error('third dimension of the network matrix should be the same as the amound of frequencies')
                            end
                        else
                            NM.matrix = repmat(data,[1 1 length(NM.Freq)]);
                        end
                    else
                        error('the network matrix should be square')
                    end
                end
            end % fast
        end % function
        
        %% change type
        function newNM = changeType(NM,newtype,newZ0)
            if any(strcmp(newtype,NM.possibleTypes))
                if strcmp(newtype,'S')||strcmp(newtype,'T')
                    % if you are coming from S or T, Z0 is saved in the object
                    if iswaves(NM)
                        newmatrix=convertRepresentation(NM.matrix,NM.type,newtype,NM.Z0);
                        newNM = NetworkMatrix(newtype,NM.freq,newmatrix,NM.Z0);
                    else
                        if exist('newZ0','var')
                            newmatrix=convertRepresentation(NM.matrix,NM.type,newtype,newZ0);
                            newNM = NetworkMatrix(newtype,NM.freq,newmatrix,newZ0);
                        else
                            error('if going to S or T matrices from a non-wave representation, Z0 should be specified')
                        end
                    end
                else
                    if iswaves(NM)
                        newmatrix=convertRepresentation(NM.matrix,NM.type,newtype,NM.Z0);
                    else
                        newmatrix=convertRepresentation(NM.matrix,NM.type,newtype);
                    end
                    newNM = NetworkMatrix(newtype,NM.Freq,newmatrix);
                end
            else
                error('new type unknown')
            end
        end
        
        %% change frequency grid
        function NM = setFreqGrid(NM,newGrid)
            % prevent extrapolation, the new grid should lie completely
            % inside the old one
            if (min(newGrid)>=min(NM.Freq))&&(max(newGrid)<=max(NM.Freq))
                temp = zeros(NM.N,NM.N,length(newGrid));
                for ii=1:NM.N
                    for jj=1:NM.N
                        temp(ii,jj,:) = interpolateAC( NM.Freq , squeeze(NM.matrix(ii,jj,:)) , newGrid );
                    end
                end
                NM.matrix = temp;
                NM.Freq = newGrid;
                NM.F = length(newGrid);
            else
                error('extrapolation not allowed');
            end
        end
        
        %% subsref override
        function B = subsref(NM,S)
            % I want to be able to address S11 as S(1,1) and get all the
            % frequencies back.
            if strcmp(S.type,'()')
                if length(S.subs)==2
                    for ii=1:2
                        if ischar(S.subs{ii}) % subs can pass a colon ':'
                            theSize = size(NM.matrix);
                            S.subs{ii}=1:theSize(1);
                        end
                    end
                    if (length(S.subs{1})==1)&&(length(S.subs{2})==1)
                        B = squeeze(NM.matrix(S.subs{1},S.subs{2},:));
                    else
                        B = NM.matrix(S.subs{1},S.subs{2},:);
                    end
                else
                    error('network matrices are 2d matrices')
                end
            else
                error('network matrices should be addressed with ()')
            end
        end
        
        %% subsasgn override
        function NM = subsasgn(NM,S,B)
            % I want to be able to assign S11 as S(1,1) = 5.
            if strcmp(S.type,'()')
                if length(S.subs)==2
                    for ii=1:2
                        if ischar(S.subs{ii}) % subs can pass a colon ':'
                            S.subs{ii}=1:length(NM.F);
                        end
                    end
                    NM.matrix(S.subs{1},S.subs{2},:) = B;
                else
                    error('network matrices are 2d matrices')
                end
            else
                error('network matrices should be addressed with ()')
            end
            
        end
        
        %% end override
        function out = end(NM,k,n)
            if n==2
                theSize = size(squeeze(NM.matrix(:,:,1)));
                out = theSize(k);
            else
                error('something wrong in end')
            end
        end
        
        %% disp override
        function disp(NM)
            disp(['     ' NM.type '-matrix'])
            if iswaves(NM)
                disp(['     Characteristic impedance: ' num2str(diag(NM.Z0)') ' Ohm']);
            end
            disp(['     At ' num2str(NM.F) ' frequency points'])
        end
        
        %% plot functions
        function plot(NM)
            colors = {'r','b','k','g','c','m'};
            markers = {'+','o','x','s'};
            num=0;
            legendentries={};
            for ii=1:NM.N
                for jj=1:NM.N
                    mar = markers{floor(num/length(colors))+1};
                    col = colors{mod(num,length(colors))+1};
                    plot(NM.Freq,squeeze(NM.matrix(ii,jj,:)),[col mar '-']);
                    if (ii==1)&&(jj==1)
                        hold on
                    end
                    legendentries{end+1}=[num2str(ii) num2str(jj)];
                    num=num+1;
                end
            end
            legend(legendentries)
        end
        
        function semilogx(NM)
            colors = {'r','b','k','g','c','m'};
            markers = {'+','o','x','s'};
            num=0;
            legendentries={};
            
            for ii=1:NM.N
                for jj=1:NM.N
                    mar = markers{floor(num/length(colors))+1};
                    col = colors{mod(num,length(colors))+1};
                    semilogx(NM.Freq,squeeze(NM.matrix(ii,jj,:)),[col mar '-']);
                    if (ii==1)&&(jj==1)
                        hold on
                    end
                    legendentries{end+1}=[num2str(ii) num2str(jj)];
                    num=num+1;
                end
            end
        end
        
        %% db and phase override
        function res = db(NM)
            if iswaves(NM)
                res = NetworkMatrix(NM.type,NM.Freq,db(NM.matrix),NM.Z0,1);
            else
                res = NetworkMatrix(NM.type,NM.Freq,db(NM.matrix),0,1);
            end
        end
        
        function res = phase(NM)
            if iswaves(NM)
                res = NetworkMatrix(NM.type,NM.Freq,angle(NM.matrix),NM.Z0,1);
            else
                res = NetworkMatrix(NM.type,NM.Freq,angle(NM.matrix),0,1);
            end
        end
        
        %% class overrice
        function res=class(NM)
            %             res = [NM.type '-matrix at ' num2str(NM.F) ' freqs'];
            res = [NM.type '-matrix'];
        end
        
        %% plus override
        function res = plus(NM1,NM2)
            % check the types first.
            
            % possibilities are:
            %           NM + NM
            %
            %           NM + NxN matrix
            %   NxN matrix + NM
            %
            %           NM + scalar
            %       scalar + NM
            disp('calling the PLUS function in NetworkMatrix')
            disp('first input argument:')
            disp(NM1)
            disp('second input argument:')
            disp(NM2)
            disp('')
            
            if isnumeric(NM1)
                if isnumeric(NM2)
                    % both are matrices, this should not happen
                    disp('this is impossible')
                else
                    % NM1 is a matrix, NM2 is a NetworkMatrix
                    if isscalar(NM1)
                        % NM1 is a scalar, NM2 is a NetworkMatrix
                        if iswaves(NM2)
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM2.matrix+NM1 ,NM2.Z0,1);
                        else
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM2.matrix+NM1 ,0,1);
                        end
                    else
                        % NM1 is a matrix, NM2 is a NetworkMatrix
                        [n1,n2,n3] = size(NM1);
                        if (n1==NM2.N)&&(n2==NM2.N)
                            if n3==1
                                % NM1 is an NxNx1 matrix, NM2 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM2.matrix));
                                for ff=1:NM2.F
                                    temp(:,:,ff) = NM1 + NM2.matrix(:,:,ff);
                                end
                                if iswaves(NM2)
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                else
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM2.F
                                    % NM1 is an NxNxF matrix, NM2 is an NxNxF NetworkMatrix
                                    if iswaves(NM2)
                                        res = NetworkMatrix(NM2.type,NM2.Freq, NM1 + NM2.matrix ,NM2.Z0,1);
                                    else
                                        res = NetworkMatrix(NM2.type,NM2.Freq, NM1 + NM2.matrix ,0,1);
                                    end
                                else
                                    error('in +')
                                end
                            end
                        else
                            error('in +');
                        end
                    end
                end
            else
                if isnumeric(NM2)
                    % NM2 is a matrix, NM1 is not
                    if isscalar(NM2)
                        % NM2 is a scalar, NM1 is a NetworkMatrix
                        if iswaves(NM1)
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix+NM2 ,NM1.Z0,1);
                        else
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix+NM2 ,0,1);
                        end
                    else
                        % NM2 is a matrix, NM1 is a NetworkMatrix
                        [n1,n2,n3] = size(NM2);
                        if (n1==NM1.N)&&(n2==NM1.N)
                            if n3==1
                                % NM2 is an NxNx1 matrix, NM1 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM1.matrix));
                                for ff=1:NM1.F
                                    temp(:,:,ff) = NM2 + NM1.matrix(:,:,ff);
                                end
                                if iswaves(NM1)
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                else
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM1.F
                                    % NM2 is an NxNxF matrix, NM1 is an NxNxF NetworkMatrix
                                    if iswaves(NM1)
                                        res = NetworkMatrix(NM1.type,NM1.Freq, NM2 + NM1.matrix ,NM1.Z0,1);
                                    else
                                        res = NetworkMatrix(NM1.type,NM1.Freq, NM2 + NM1.matrix ,0,1);
                                    end
                                else
                                    error('in +')
                                end
                            end
                        else
                            error('in +');
                        end
                    end
                else
                    % NM1 and NM2 are both NetworkMatrices
                    
                    % the type should be the same
                    if strcmp(NM1.type,NM2.type)
                        % the frequency axis should be the same
                        if NM1.F==NM2.F % is there a better way to check this? the actual frequencies should be the same, not the length of the vector
                            % the amount of ports should be the same
                            if NM1.N == NM2.N
                                if iswaves(NM1)
                                    if NM1.Z0==NM2.Z0
                                        res = NetworkMatrix(NM1.type,NM1.Freq, NM2.matrix + NM1.matrix ,NM1.Z0,1);
                                    else
                                        error('in +');
                                    end
                                else
                                    res = NetworkMatrix(NM1.type,NM1.Freq, NM2.matrix + NM1.matrix ,0,1);
                                end
                            else
                                error('in +');
                            end
                        else
                            error('in +');
                        end
                    else
                        error('in +');
                    end
                end
            end
        end
        
        %% minus override
        function res = minus(NM1,NM2)
            
            % check the types first.
            
            % possibilities are:
            %           NM - NM
            %
            %           NM - NxN matrix
            %   NxN matrix - NM
            %
            %           NM - scalar
            %       scalar - NM
            disp('calling the MINUS function in NetworkMatrix')
            disp('first input argument:')
            disp(NM1)
            disp('second input argument:')
            disp(NM2)
            disp('')
            
            if isnumeric(NM1)
                if isnumeric(NM2)
                    % both are matrices, this should not happen
                    disp('this is impossible')
                else
                    % NM1 is a matrix, NM2 is a NetworkMatrix
                    if isscalar(NM1)
                        % NM1 is a scalar, NM2 is a NetworkMatrix
                        if iswaves(NM2)
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1-NM2.matrix ,NM2.Z0,1);
                        else
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1-NM2.matrix ,0,1);
                        end
                    else
                        % NM1 is a matrix, NM2 is a NetworkMatrix
                        [n1,n2,n3] = size(NM1);
                        if (n1==NM2.N)&&(n2==NM2.N)
                            if n3==1
                                % NM1 is an NxNx1 matrix, NM2 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM2.matrix));
                                for ff=1:NM2.F
                                    temp(:,:,ff) = NM1 - NM2.matrix(:,:,ff);
                                end
                                if iswaves(NM2)
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                else
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM2.F
                                    % NM1 is an NxNxF matrix, NM2 is an NxNxF NetworkMatrix
                                    if iswaves(NM2)
                                        res = NetworkMatrix(NM2.type,NM2.Freq, NM1 - NM2.matrix ,NM2.Z0,1);
                                    else
                                        res = NetworkMatrix(NM2.type,NM2.Freq, NM1 - NM2.matrix ,0,1);
                                    end
                                else
                                    error('in -')
                                end
                            end
                        else
                            error('in -');
                        end
                    end
                end
            else
                if isnumeric(NM2)
                    % NM2 is a matrix, NM1 is not
                    if isscalar(NM2)
                        % NM2 is a scalar, NM1 is a NetworkMatrix
                        if iswaves(NM1)
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix-NM2 ,NM1.Z0,1);
                        else
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix-NM2 ,0,1);
                        end
                    else
                        % NM2 is a matrix, NM1 is a NetworkMatrix
                        [n1,n2,n3] = size(NM2);
                        if (n1==NM1.N)&&(n2==NM1.N)
                            if n3==1
                                % NM2 is an NxNx1 matrix, NM1 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM1.matrix));
                                for ff=1:NM1.F
                                    temp(:,:,ff) = NM1.matrix(:,:,ff) - NM2;
                                end
                                if iswaves(NM1)
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                else
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM1.F
                                    % NM2 is an NxNxF matrix, NM1 is an NxNxF NetworkMatrix
                                    if iswaves(NM1)
                                        res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix - NM2 ,NM1.Z0,1);
                                    else
                                        res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix - NM2 ,0,1);
                                    end
                                else
                                    error('in -')
                                end
                            end
                        else
                            error('in -');
                        end
                    end
                else
                    % NM1 and NM2 are both NetworkMatrices
                    
                    % the type should be the same
                    if strcmp(NM1.type,NM2.type)
                        % the frequency axis should be the same
                        if NM1.F==NM2.F % is there a better way to check this? the actual frequencies should be the same, not the length of the vector
                            % the amount of ports should be the same
                            if NM1.N == NM2.N
                                if iswaves(NM1)
                                    if NM1.Z0==NM2.Z0
                                        res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix - NM2.matrix ,NM1.Z0,1);
                                    else
                                        error('in -');
                                    end
                                else
                                    res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix - NM2.matrix ,0,1);
                                end
                            else
                                error('in -');
                            end
                        else
                            error('in -');
                        end
                    else
                        error('in -');
                    end
                end
            end
        end
        
        %% mtimes override
        function res = mtimes(NM1,NM2)
            % possibilities are:
            %       NM * NM         both NM should have same F, N and Z0
            %   
            %   scalar * NM
            %       NM * scalar
            %
            %   matrix * NM         matrix can be NxN or NxNxF
            %       NM * matrix
            %
            %       NM * vector     vector should be column and have length N.
            %                       in this case the function returns a N x F matrix.
            %
            % I don't think vector * NM is necessary
            
            disp('calling the MTIMES function in NetworkMatrix')
            disp('first input argument:')
            disp(NM1)
            disp('second input argument:')
            disp(NM2)
            disp('')
            
            if isnumeric(NM1)
                if isnumeric(NM2)
                    % both are matrices, this should not happen
                    disp('this is impossible')
                else
                    % NM1 is a matrix, NM2 is a NetworkMatrix
                    if isscalar(NM1)
                        % NM1 is a scalar, NM2 is a NetworkMatrix
                        if iswaves(NM2)
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1*NM2.matrix ,NM2.Z0,1);
                        else
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1*NM2.matrix ,0,1);
                        end
                    else
                        % NM1 is a matrix, NM2 is a NetworkMatrix
                        [n1,n2,n3] = size(NM1);
                        if (n1==NM2.N)&&(n2==NM2.N)
                            if n3==1
                                % NM1 is an NxNx1 matrix, NM2 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM2.matrix));
                                for ff=1:NM2.F
                                    temp(:,:,ff) = NM1 * NM2.matrix(:,:,ff);
                                end
                                if iswaves(NM2)
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                else
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM2.F
                                    % NM1 is an NxNxF matrix, NM2 is an NxNxF NetworkMatrix
                                    
                                    temp = zeros(size(NM2.matrix));
                                    for ff=1:NM2.F
                                        temp(:,:,ff) = NM1(:,:,ff) * NM2.matrix(:,:,ff);
                                    end
                                    
                                    if iswaves(NM2)
                                        res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                    else
                                        res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                    end
                                    
                                else
                                    error('in *')
                                end
                            end
                        else
                            error('in *');
                        end
                    end
                end
            else
                if isnumeric(NM2)
                    % NM2 is a matrix, NM1 is not
                    if isscalar(NM2)
                        % NM2 is a scalar, NM1 is a NetworkMatrix
                        if iswaves(NM1)
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix*NM2 ,NM1.Z0,1);
                        else
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix*NM2 ,0,1);
                        end
                    else
                        % NM2 is a matrix, NM1 is a NetworkMatrix
                        [n1,n2,n3] = size(NM2);
                        if (n1==NM1.N)&&(n2==NM1.N)
                            if n3==1
                                % NM2 is an NxNx1 matrix, NM1 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM1.matrix));
                                for ff=1:NM1.F
                                    temp(:,:,ff) = NM1.matrix(:,:,ff) * NM2;
                                end
                                if iswaves(NM1)
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                else
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM1.F
                                    % NM2 is an NxNxF matrix, NM1 is an NxNxF NetworkMatrix
                                    temp = zeros(size(NM1.matrix));
                                    for ff=1:NM1.F
                                        temp(:,:,ff)=NM1.matrix(:,:,ff) * NM2(:,:,ff);
                                    end
                                    if iswaves(NM1)
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                    else
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                    end
                                else
                                    error('in *')
                                end
                            end
                        else
                            % 
                            if n1==NM1.N
                                % TODO, vector multiplication should be
                                % supported.
                                % maybe the vector is a Nx1xF or maybe it
                                % is a NxF, so either n2 or n3 is equal to
                                % F
                                if n2==1
                                    if n3==1
                                        % NM2 is an Nx1x1 vector
                                        res = zeros(NM1.N,NM1.F);
                                        for ff=1:NM1.F
                                            res(:,ff) = NM1.matrix(:,:,ff)*NM2;
                                        end
                                    elseif n3==NM1.F
                                        % NM2 is a Nx1xF vector
                                        res = zeros(NM1.N,NM1.F);
                                        for ff=1:NM1.F
                                            res(:,ff) = NM1.matrix(:,:,ff)*NM2(:,1,ff);
                                        end
                                    else
                                        error('in *');
                                    end
                                else
                                    if n2==NM.F
                                        if n3==1
                                            % NM2 is an NxF vector
                                            res = zeros(NM1.N,NM1.F);
                                            for ff=1:NM1.F
                                                res(:,ff) = NM1.matrix(:,:,ff)*NM2(:,ff);
                                            end
                                        else
                                            error('in *');
                                        end
                                    else
                                        error('in *')
                                    end
                                end
                            else
                                error('in *');
                            end 
                        end
                    end
                else
                    % NM1 and NM2 are both NetworkMatrices
                    
                    % the type should be the same
                    if strcmp(NM1.type,NM2.type)
                        % the frequency axis should be the same
                        if NM1.F==NM2.F % is there a better way to check this? the actual frequencies should be the same, not the length of the vector
                            % the amount of ports should be the same
                            if NM1.N == NM2.N
                                if iswaves(NM1)
                                    if NM1.Z0==NM2.Z0
                                        temp = zeros(size(NM1.matrix));
                                        for ff=1:NM1.F
                                            temp(:,:,ff)=NM1.matrix(:,:,ff)*NM2.matrix(:,:,ff);
                                        end
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                    else
                                        error('in *');
                                    end
                                else
                                    temp = zeros(size(NM1.matrix));
                                    for ff=1:NM1.F
                                        temp(:,:,ff)=NM1.matrix(:,:,ff)*NM2.matrix(:,:,ff);
                                    end
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                error('in *');
                            end
                        else
                            error('in *');
                        end
                    else
                        error('in *');
                    end
                end
            end
        end
        
        %% mldivide override
        function res = mldivide(NM1,NM2)
            % possibilities are:
            %       NM \ NM         both NM should have same F, N and Z0
            %   
            %   scalar \ NM
            %       NM \ scalar
            %
            %   matrix \ NM         matrix can be NxN or NxNxF
            %       NM \ matrix
            %
            
            disp('calling the MLDIVIDE function in NetworkMatrix')
            disp('first input argument:')
            disp(NM1)
            disp('second input argument:')
            disp(NM2)
            disp('')
            
            if isnumeric(NM1)
                if isnumeric(NM2)
                    % both are matrices, this should not happen
                    disp('this is impossible!')
                else
                    % NM1 is a matrix, NM2 is a NetworkMatrix
                    if isscalar(NM1)
                        % NM1 is a scalar, NM2 is a NetworkMatrix
                        if iswaves(NM2)
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1\NM2.matrix ,NM2.Z0,1);
                        else
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1\NM2.matrix ,0,1);
                        end
                    else
                        % NM1 is a matrix, NM2 is a NetworkMatrix
                        [n1,n2,n3] = size(NM1);
                        if (n1==NM2.N)&&(n2==NM2.N)
                            if n3==1
                                % NM1 is an NxNx1 matrix, NM2 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM2.matrix));
                                for ff=1:NM2.F
                                    temp(:,:,ff) = NM1 \ NM2.matrix(:,:,ff);
                                end
                                if iswaves(NM2)
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                else
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM2.F
                                    % NM1 is an NxNxF matrix, NM2 is an NxNxF NetworkMatrix
                                    
                                    temp = zeros(size(NM2.matrix));
                                    for ff=1:NM2.F
                                        temp(:,:,ff) = NM1(:,:,ff) \ NM2.matrix(:,:,ff);
                                    end
                                    
                                    if iswaves(NM2)
                                        res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                    else
                                        res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                    end
                                    
                                else
                                    error('in \')
                                end
                            end
                        else
                            error('in \');
                        end
                    end
                end
            else
                if isnumeric(NM2)
                    % NM2 is a matrix, NM1 is not
                    if isscalar(NM2)
                        % NM2 is a scalar, NM1 is a NetworkMatrix
                        if iswaves(NM1)
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix\NM2 ,NM1.Z0,1);
                        else
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix\NM2 ,0,1);
                        end
                    else
                        % NM2 is a matrix, NM1 is a NetworkMatrix
                        [n1,n2,n3] = size(NM2);
                        if (n1==NM1.N)&&(n2==NM1.N)
                            if n3==1
                                % NM2 is an NxNx1 matrix, NM1 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM1.matrix));
                                for ff=1:NM1.F
                                    temp(:,:,ff) = NM1.matrix(:,:,ff) \ NM2;
                                end
                                if iswaves(NM1)
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                else
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM1.F
                                    % NM2 is an NxNxF matrix, NM1 is an NxNxF NetworkMatrix
                                    temp = zeros(size(NM1.matrix));
                                    for ff=1:NM1.F
                                        temp(:,:,ff)=NM1.matrix(:,:,ff) \ NM2(:,:,ff);
                                    end
                                    if iswaves(NM1)
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                    else
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                    end
                                else
                                    error('in \')
                                end
                            end
                        else
                            error('in \')
                        end
                    end
                else
                    % NM1 and NM2 are both NetworkMatrices
                    
                    % the type should be the same
                    if strcmp(NM1.type,NM2.type)
                        % the frequency axis should be the same
                        if NM1.F==NM2.F % is there a better way to check this? the actual frequencies should be the same, not the length of the vector
                            % the amount of ports should be the same
                            if NM1.N == NM2.N
                                if iswaves(NM1)
                                    if NM1.Z0==NM2.Z0
                                        temp = zeros(size(NM1.matrix));
                                        for ff=1:NM1.F
                                            temp(:,:,ff)=NM1.matrix(:,:,ff) \ NM2.matrix(:,:,ff);
                                        end
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                    else
                                        error('in \');
                                    end
                                else
                                    temp = zeros(size(NM1.matrix));
                                    for ff=1:NM1.F
                                        temp(:,:,ff)=NM1.matrix(:,:,ff)\NM2.matrix(:,:,ff);
                                    end
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                error('in \');
                            end
                        else
                            error('in \');
                        end
                    else
                        error('in \');
                    end
                end
            end
        end
        
        %% mrdivide override
        function res = mrdivide(NM1,NM2)
            % possibilities are:
            %       NM / NM         both NM should have same F, N and Z0
            %   
            %   scalar / NM
            %       NM / scalar
            %
            %   matrix / NM         matrix can be NxN or NxNxF
            %       NM / matrix
            
            disp('calling the MRDIVIDE function in NetworkMatrix')
            disp('first input argument:')
            disp(NM1)
            disp('second input argument:')
            disp(NM2)
            disp('')
            
            if isnumeric(NM1)
                if isnumeric(NM2)
                    % both are matrices, this should not happen
                    disp('this is impossible!')
                else
                    % NM1 is a matrix, NM2 is a NetworkMatrix
                    if isscalar(NM1)
                        % NM1 is a scalar, NM2 is a NetworkMatrix
                        if iswaves(NM2)
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1/NM2.matrix ,NM2.Z0,1);
                        else
                            res = NetworkMatrix(NM2.type,NM2.Freq, NM1/NM2.matrix ,0,1);
                        end
                    else
                        % NM1 is a matrix, NM2 is a NetworkMatrix
                        [n1,n2,n3] = size(NM1);
                        if (n1==NM2.N)&&(n2==NM2.N)
                            if n3==1
                                % NM1 is an NxNx1 matrix, NM2 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM2.matrix));
                                for ff=1:NM2.F
                                    temp(:,:,ff) = NM1 / NM2.matrix(:,:,ff);
                                end
                                if iswaves(NM2)
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                else
                                    res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM2.F
                                    % NM1 is an NxNxF matrix, NM2 is an NxNxF NetworkMatrix
                                    
                                    temp = zeros(size(NM2.matrix));
                                    for ff=1:NM2.F
                                        temp(:,:,ff) = NM1(:,:,ff) / NM2.matrix(:,:,ff);
                                    end
                                    
                                    if iswaves(NM2)
                                        res = NetworkMatrix(NM2.type,NM2.Freq, temp ,NM2.Z0,1);
                                    else
                                        res = NetworkMatrix(NM2.type,NM2.Freq, temp ,0,1);
                                    end
                                    
                                else
                                    error('in /')
                                end
                            end
                        else
                            error('in \');
                        end
                    end
                end
            else
                if isnumeric(NM2)
                    % NM2 is a matrix, NM1 is not
                    if isscalar(NM2)
                        % NM2 is a scalar, NM1 is a NetworkMatrix
                        if iswaves(NM1)
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix/NM2 ,NM1.Z0,1);
                        else
                            res = NetworkMatrix(NM1.type,NM1.Freq, NM1.matrix/NM2 ,0,1);
                        end
                    else
                        % NM2 is a matrix, NM1 is a NetworkMatrix
                        [n1,n2,n3] = size(NM2);
                        if (n1==NM1.N)&&(n2==NM1.N)
                            if n3==1
                                % NM2 is an NxNx1 matrix, NM1 is an NxNxF NetworkMatrix
                                temp = zeros(size(NM1.matrix));
                                for ff=1:NM1.F
                                    temp(:,:,ff) = NM1.matrix(:,:,ff) / NM2;
                                end
                                if iswaves(NM1)
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                else
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                if n3==NM1.F
                                    % NM2 is an NxNxF matrix, NM1 is an NxNxF NetworkMatrix
                                    temp = zeros(size(NM1.matrix));
                                    for ff=1:NM1.F
                                        temp(:,:,ff)=NM1.matrix(:,:,ff) / NM2(:,:,ff);
                                    end
                                    if iswaves(NM1)
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                    else
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                    end
                                else
                                    error('in /')
                                end
                            end
                        else
                            error('in /')
                        end
                    end
                else
                    % NM1 and NM2 are both NetworkMatrices
                    
                    % the type should be the same
                    if strcmp(NM1.type,NM2.type)
                        % the frequency axis should be the same
                        if NM1.F==NM2.F % is there a better way to check this? the actual frequencies should be the same, not the length of the vector
                            % the amount of ports should be the same
                            if NM1.N == NM2.N
                                if iswaves(NM1)
                                    if NM1.Z0==NM2.Z0
                                        temp = zeros(size(NM1.matrix));
                                        for ff=1:NM1.F
                                            temp(:,:,ff)=NM1.matrix(:,:,ff)/NM2.matrix(:,:,ff);
                                        end
                                        res = NetworkMatrix(NM1.type,NM1.Freq, temp ,NM1.Z0,1);
                                    else
                                        error('in /');
                                    end
                                else
                                    temp = zeros(size(NM1.matrix));
                                    for ff=1:NM1.F
                                        temp(:,:,ff)=NM1.matrix(:,:,ff) / NM2.matrix(:,:,ff);
                                    end
                                    res = NetworkMatrix(NM1.type,NM1.Freq, temp ,0,1);
                                end
                            else
                                error('in /');
                            end
                        else
                            error('in /');
                        end
                    else
                        error('in /');
                    end
                end
            end
        end
        
        %% ctranspose override
        function res=ctranspose(NM1)
            temp = zeros(size(NM1.matrix));
            for ii=1:NM1.F
                temp(:,:,ii)=NM1.matrix(:,:,ii)';
            end
            
            if iswaves(NM1)
                res = NetworkMatrix(NM1.type , NM1.Freq , temp , NM1.Z0 , 1 );
            else
                res = NetworkMatrix(NM1.type , NM1.Freq , temp, 0 , 1 );
            end
        end
        
        %% override transpose
        function res=transpose(NM1)
            temp = zeros(size(NM1.matrix));
            for ii=1:NM1.F
                temp(:,:,ii)=NM1.matrix(:,:,ii).';
            end
            
            if iswaves(NM1)
                res = NetworkMatrix(NM1.type , NM1.Freq , temp , NM1.Z0 , 1 );
            else
                res = NetworkMatrix(NM1.type , NM1.Freq , temp, 0 , 1 );
            end
        end

        %% iswaves
        % checks whether the type is S or T, then a characteristic
        % impedance Z0 is needed.
        function res = iswaves(NM)
            res = strcmp(NM.type,'S')||strcmp(NM.type,'T');
        end
        
        %% is* functions
        function res = ismatrix(NM)
            res = 0;
        end
        
        function res=isnumeric(NM)
            res=0;
        end
        
        %% getters
        function data = getData(NM)
            data=NM.matrix;
        end
        
        function freq = getFreq(NM)
            freq = NM.Freq;
        end
        
        function Z0 = getCharacteristic(NM)
            Z0 = NM.Z0;
        end
        
        
    end % methods
end % classdef