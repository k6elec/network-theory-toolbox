function res = suppressPorts(type,A,p)
% SUPPRESSPORTS Leave ports open in a network
%
%   res = suppressPorts(type,A,p)
%
% Inputs
%   type    Char representing the type of matrix given to the function.
%           Options are 'Y' or 'Z'.
%   Y       An N x N x F matrix representing the N-port system over F
%           frequencies
%   p       An 1 x M vector which contains the ports that are suppressed
%
% Outputs 
%   res     an N-M x N-M x F matrix
%

% check the inputs
[A,F]=checkYparamInput(A);

ports = length(p);
res = zeros(N-ports,N-ports,F);

if strcmp(type,'Y')
    for ii=1:F
        % put the terminals to be suppressed on the bottom
        Ytemp = rijen_permutatie(A(:,:,ii),p);
        % split the Y-matrix into parts
        [Ypp,Ycc,Ycp,Ypc]=split(Ytemp,ports);
        % calculate the new Y-matrix
        res(:,:,ii) = Ypp - Ypc * ((Ycc)^-1) * Ycp;
    end 
elseif strcmp(type,'Z') 
    for ii=1:F
        % put the terminals to be suppressed on the bottom
        Ztemp = rijen_permutatie(A(:,:,ii),p);
        % split the Y-matrix into parts
        [Zpp,~,~,~]=split(Ztemp,ports);
        % calculate the new Y-matrix
        res(:,:,ii) = Zpp;
    end
else
    error('type specified is unknown.')
end



%% function to split the sorted S-matrix into parts
    function [pp,cc,cp,pc]=split(S,par)
        pp = S(1:par,1:par);
        cc = S((par+1):end,(par+1):end);
        cp = S((par+1):end,1:par);
        pc = S(1:par,(par+1):end);
    end





end