function drawcircle(center,rad,points)
% draws a circle on a smith chart
%
% drawcircle(center,rad)
% drawcircle(center,rad,points)
%
% where center is the complex number indicating the center and rad is the
% radius.
% points is the amount of points on the circle. default=100
%
% Adam Cooman, ELEC VUB

if ~exist('points','var')
    points=100;
end

phasevec=(0:points-1)/points*2*pi;

smithchart(center+rad*exp(1i*phasevec))

end