function Yres = connectPorts(Y,p)
% CONNECTPORTS connects two ports of an Y-matrix representation to present one port.
%   
%   Yres = connectPorts(Y,p)
%
% Inputs:
%   Y       N x N Y-matrix of the system
%   p       M x 1 vector which contains the ports that are to be 
%           connected together 
%
% Output
%   Yres 	N-M+1 x N-M+1 matrix where the new port is placed last
%
% Notes:
% ref: "Electrical Network Theory" N. Balabanian
%   Suppose two terminals of an N-port network are connected together into
%   a single external terminal. The two external currents are replaced by
%   one, equal to the sum of the original two. The two voltages are now
%   identical. Hence the Yi-matrix of the resulting (n-1) terminal network
%   is obtained by adding the two corresponding rows and columns of the
%   original Yi-matrix.
% 
% This function is old and likely to disappear
%
% See also CONNECTSMATRICES CONNECTZMATRICES 
%
% Version History:
% Adam Cooman, ELEC VUB

[Y,F,N]=checkYparamInput(Y);
ports = length(p);

M = N-ports;
Yres = zeros(N-ports+1,N-ports+1,F);

for ii=1:F
    
Yper = rijen_permutatie(Y(:,:,ii),p);
Yres(1:M,1:M)=Yper(1:M,1:M);
Yres(1:M,M+1)=sum(Yper(1:M,M+1:N),2);
Yres(M+1,1:M)=sum(Yper(M+1:N,1:M),1);
Yres(M+1,M+1)=sum(sum(Yper(M+1:N,M+1:N)));

end



end