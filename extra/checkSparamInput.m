function [S,Z0,Y0,F]=checkSparamInput(S,Zchar)
% internal function used to check the inputs of the S-parameter conversion
% functions

[N,M,F]=size(S);
if N~=M
    error('network-matrix should be sqare')
end

if isscalar(Zchar)
    % Zchar is a scalar, make it into a diagonal matrix
    Z0 = Zchar*eye(N);
    Y0 = sqrt((1./Zchar)*eye(N));
    Z0 = sqrt(Z0);
else
    sizeZchar=size(Zchar);
    if max(sizeZchar)~=N
        error('if multiple characteristic impedances are specified, the amount should be the same as the amount of ports in the system')
    end
    if min(sizeZchar)==1
        % Zchar is a vector
        Z0= diag(sqrt(Zchar));
        Y0 = diag(1./sqrt(Zchar));
    else
        % Zchar is a matrix
        Z0 = sqrt(Zchar);
        Y0 = diag(1./sqrt(diag(Zchar)));
    end
end

end