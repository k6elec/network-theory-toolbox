function [A,F,N]=checkYparamInput(A)

[N,M,F]=size(A);

% check for a square matrix
if N~=M
    error('matrix should be square')
end


end