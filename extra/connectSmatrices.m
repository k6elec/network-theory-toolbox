function res = connectSmatrices(S1 , S2 , p1 , p2 ,mode)
% CONNECTSMATRICES connects an M-port S-matrix with an N-port S-matrix.
%
%       S = connectSmatrices(S1 , S2 , p1 , p2, mode )
%
% Inputs
%   S1      is an N x N x F S-matrix describing the first system
%   S2      in an M x M x F S-matrix describing the second system
%   p1      is the vector of ports to be connected of S1. length(p1)<=N
%   p2      is the vector of ports to be connected of S2. length(p2)<=M
%           - p1 and p2 should have the same length
%           - p1 can only contain integers between 1 and M
%           - p2 can only contain integers between 1 and N
%   mode    ???
%
% Output
%   S       Is an S matrix which represents the interconnected system.
%           it is a square matrix of size N + M - length(p1). The remaining 
%           ports of the first system are placed first, followed by the 
%           remaining ports of the second system.
%


[S1,F1] = checkYparamInput(S1);
[S2,F2] = checkYparamInput(S2);

if ~exist('mode','var')
    mode=0;
end

if F1~=F2
    error('both matrices should be specified at the same frequencies');
else
    F=F1;
end

% get the needed sizes
[M,~,~] = size(S1);
[N,~,~] = size(S2);
c = length(p1);
p = M - c;
q = N - c;


if mode~=1
    
    res = zeros(p+q,p+q,F);
    for ii=1:F
        
        % Sort the matrices such that the ports to be connected are put last.
        S1t = rijen_permutatie(S1(:,:,ii),p1);
        S2t = rijen_permutatie(S2(:,:,ii),p2);
        
        % split both S-matrices into parts
        [S1pp,S1cc,S1cp,S1pc] = split(S1t,p);
        [S2qq,S2cc,S2cq,S2qc] = split(S2t,q);
        
        % apply (2.56b) to the parts of the matrices to obtain the result
        res(:,:,ii) = [S1pp zeros(p,q) ; zeros(q,p) S2qq] + ...
            [ S1pc zeros(p,c); zeros(q,c) S2qc] * ...
            ( [-S1cc eye(c) ; eye(c) -S2cc ] )^-1 * ...
            [S1cp zeros(c,q) ; zeros(c,p) S2cq];
        
    end
    
else
        % THIS IMPLEMENTATION STILL HAS TO BE TESTED THOUROUGHLY !!!!
        
        % Sort the matrices such that the ports to be connected are put last.
        S1t = rijen_permutatie(S1,p1);
        S2t = rijen_permutatie(S2,p2);
        
        % split both S-matrices into parts
        [S1pp,S1cc,S1cp,S1pc] = split_vectorised(S1t,p);
        [S2qq,S2cc,S2cq,S2qc] = split_vectorised(S2t,q);
        
        % apply (2.56b) to the parts of the matrices to obtain the result
        
        A = zeros(p+q,p+q,F);
        A(1:p,1:p,:)=S1pp;
        A(p+1:end,p+1:end,:)=S2qq;
        
        B = zeros(p+q,2*c,F);
        B(1:p,1:c,:)=S1pc;
        B(c+1:end,p+1:end,:)=S2qc;
        
        C = zeros(2*c,c*2,F);
        C(1:c    ,1:c    ,:)=-S1cc;
        C(c+1:end,c+1:end,:)=-S2cc;
        C(c+1:end,1:c    ,:)=repmat(eye(c),[1,1,F]);
        C(1:c    ,c+1:end,:)=repmat(eye(c),[1,1,F]);
        
        D = zeros(2*c,p+q,F);
        D(1:c,1:p)=S1cp;
        D(c+1:end,p+1:end)=S2cq;
        
        % S = A + B*inv(C)*D
        temp = multinv(C);
        temp = multiprod(B,temp);
        temp = multiprod(temp,D);
        res = A + temp;

end


%% function to split the sorted S-matrix into parts
    function [pp,cc,cp,pc]=split(S,par)
        pp = S(1:par,1:par);
        cc = S((par+1):end,(par+1):end);
        cp = S((par+1):end,1:par);
        pc = S(1:par,(par+1):end);
    end

    function [pp,cc,cp,pc]=split_vectorised(S,par)
        pp = S(1:par,1:par,:);
        cc = S((par+1):end,(par+1):end,:);
        cp = S((par+1):end,1:par,:);
        pc = S(1:par,(par+1):end,:);    
    end

end