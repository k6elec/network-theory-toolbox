function [St,CvecSt] = unbal2bal(varargin)
% transforms a multiport S matrix into a generalised mixed-mode S-matrix
%
% S = unbal2bal(S)
% S = unbal2bal(S,mixedPorts)
% S = unbal2bal(S,mixedPorts,portinimp,portoutimp)
% [St,CvecSt] = unbal2bal(S,mixedPorts,portinimp,portoutimp,CvecS)
%
% S is an NxNxF matrix with F the amound of frequency points.
%
% mixedPorts is the px2 matrix which indicates the ports that should be
% connected together in a mixed-mode S-parameter.
% The remaining ports are considered as normal ports.
% If the mixedPort matrix is not passed, it is assumed all ports are
% differential and pairs of ports are taken from top to bottom:
% For example: if a 4-port S-matrix is provided, ports 1 and 2 are mixed
% together and ports 3 and 4 also.
%
% The resulting S-matrix will have its ports in the following order:
% | diff ports   |
% | comm ports   |
% | single ports |
%
% portsinimp is the vector with the port impedances of the input S-matrix.
%       The default is 50Ohm for all ports.
% portsoutimp is the vector with the port impedances of the output S-matrix
%       The default is 100Ohm for the differential ports, 25Ohm for the
%       common-mode ports and 50Ohm for the remaining single-ended ports.
% The order of portsoutimp is the same as the way the ports are ordered in
% the output S-matrix.
%
% CvecS is an N*NxN*NxF matrix that contains the covariance of vecS
%
% Based on the algorithm described in:
% A. Ferrero and M. Pirola. "Generalised Mixed-Mode S-parameters" IEEE
% Tran. on Microwave Theory and Techiques, Vol. 54 No. 1, Jan. 2006
%
% Adam Cooman, ELEC VUB
%   12-11-2013  Version 1
%   28-08-2015  added the transformation of CvecS


%% for testing purposes, you can uncomment the following stuff
% clear all
% clc
% % test the default settings by inserting an ideal differential line
% % 1 o-------o 3
% % 2 o-------o 4
% % S = [0 0 1 0;0 0 0 1;1 0 0 0;0 1 0 0];
% 
% % test the port re-ordering by specifying the different mixed ports
% % 1 o-------o 2
% % 3 o-------o 4
% S = [0 1 0 0;1 0 0 0;0 0 0 1;0 0 1 0];
% mixedPorts=[1 3;2 4];


% S,mixedPorts,portinimp,portoutimp,CvecS
p=inputParser();
p.addRequired('S',@isnumeric);
p.addOptional('mixedPorts',[],@ismatrix);
p.addOptional('portinimp',[],@isvector)
p.addOptional('portoutimp',[],@(x) isvector(x)||isempty(x))
p.addOptional('CvecS',[],@isnumeric)
p.parse(varargin{:});
args = p.Results;
S = args.S;
if ~isempty(args.mixedPorts);mixedPorts=args.mixedPorts;end
if ~isempty(args.portinimp);portinimp=args.portinimp;end
if ~isempty(args.portoutimp);portoutimp=args.portoutimp;end
if ~isempty(args.CvecS);CvecS=args.CvecS;end
clear p args varargin

% N is the amount of ports in the circuit
% F is the amount of frequencies in the measurement
[N,~,F]=size(S);

if ~exist('mixedPorts','var')
    % if the mixedports are not provided, all ports are assumed
    % differential and adjacent ports are combined.
    if mod(N,2)~=0
        error('the amount of ports should be even.');
    end
    p = N/2;
else
    % if the mixedports are provided (and not empty), the ports are
    % re-ordered. all ports that are to be combined are placed first,
    % the remaining single-ended ports are placed last.
    if isempty(mixedPorts)         
        if mod(N,2)~=0
            error('the amount of ports should be even.');
        end
        p=N/2;
    else
        % M is the amount of mixed ports in the system
        [p,M]=size(mixedPorts);
        % do a lot of checking on the mixedPorts vector
        mixedPorts = checkMixedPorts(mixedPorts,N,M,p);
        mixedPorts = mixedPorts';
        % permute the ports into the standard order to allow the method to be applied
        if exist('CvecS','var')
            [S,~,CvecS] = rijen_permutatie(S,mixedPorts(:),0,CvecS);
        else
            S = rijen_permutatie(S,mixedPorts(:),0);
        end
    end
end

% check the input port impedance vector
if ~exist('portinimp','var')
    % default is 50Ohm for all the ports
    portinimp = 50*ones(N,1);
else
    if isempty(portinimp)
        portinimp = 50*ones(N,1);
    else
        if isscalar(portinimp)
           portinimp=portinimp*ones(N,1);
        else
            if isvector(portinimp)
                if length(portinimp)~=N
                    error('length of portinimp should be N');
                end
            else
                error('portinimp should be either a scalar or a vector.')
            end
        end
    end
end

% check the output port impedance vector
if ~exist('portoutimp','var')
    % default is 100 Ohm for the differential ports, 25 for the common-mode
    % ports and 50Ohm for the single-ended ports.
    portoutimp = [100*ones(p,1);25*ones(p,1);50*ones(N-2*p,1)]; 
else
    if isempty(portoutimp)
        portoutimp = [100*ones(p,1);25*ones(p,1);50*ones(N-2*p,1)]; 
    end
end

diffportimp = portoutimp(  1:p  );
commportimp = portoutimp(p+1:2*p);

% build the B matrix, this is the matrix in expression (24) in the Ferrero paper
% TODO: this would be more clean if it's done with blkdiag
B = zeros(2*N);
for ii=1:p
    Zj = portinimp(2*(p-1)+1);
    Zk = portinimp(2*(p-1)+2);
    Zcjk = commportimp(ii);
    Zdjk = diffportimp(ii);
    aeye = 2*eye(2)-ones(2);
    % TODO, this is the transformation for the pseudo-waves, the
    % alternative for the power-waves should also be provided.
    Xa = abs(Zj)/(4*sqrt(real(Zj))*Zk)*[ sqrt(real(Zdjk))*(2*Zj*ones(2)+Zdjk*aeye)/abs(Zdjk) ; sqrt(real(Zcjk))*(Zj*ones(2)+2*Zcjk*aeye)/abs(Zcjk)];
    Xb = abs(Zk)/(4*sqrt(real(Zk))*Zk)*[-sqrt(real(Zdjk))*(2*Zk*ones(2)+Zdjk*aeye)/abs(Zdjk) ; sqrt(real(Zcjk))*(Zk*ones(2)+2*Zcjk*aeye)/abs(Zcjk)];
    X = [Xa Xb];
    B((ii-1)*4+1:(ii)*4,(ii-1)*4+1:(ii)*4) = X;
end

% The other ports should not be transformed, so the identity matrix is added in the B-transformation matrix
if p~=(N/2)
    B((ii)*4+1:end,(ii)*4+1:end) = eye(2*(N-2*p));
end

% build the P matrix
Pda = zeros(p,2*N);
Pca = zeros(p,2*N);
Pdb = zeros(p,2*N);
Pcb = zeros(p,2*N);
for l=1:p
    Pda(l,4*l-3)=1;
    Pca(l,4*l-1)=1;
    Pdb(l,4*l-2)=1;
    Pcb(l,4*l  )=1;
end
if p~=(N/2)
    Pa = zeros(N-2*p,2*N);
    Pb = zeros(N-2*p,2*N);
    for ii=1:N-2*p
        Pa(ii,4*p+2*ii-1)=1;
        Pb(ii,4*p+2*ii)=1;
    end
else
    Pa = [];
    Pb = [];
end
P = [Pda;Pca;Pa;Pdb;Pcb;Pb];
% clear Pda Pca Pa Pdb Pcb Pb

% build the Q matrix
Qa = zeros(N,2*N);
Qb = zeros(N,2*N);
for l=1:N
    Qa(l,2*l-1) = 1;
    Qb(l,2*l  ) = 1;
end
Q = [Qa;Qb];
% clear Qa Qb

% build the Btilde matrix
Bt = P*B*Q';

% split the Btilde matrix into pieces
Bt11 = Bt(  1:N  ,  1:N);
Bt12 = Bt(  1:N  ,N+1:end);
Bt21 = Bt(N+1:end,  1:N);
Bt22 = Bt(N+1:end,N+1:end);

% transform the S-matrix
St = zeros(size(S));
for ff=1:F
    St(:,:,ff) = (Bt21 + Bt22*S(:,:,ff)) / (Bt11+Bt12*S(:,:,ff));
end

if exist('CvecS','var')
    CvecSt = zeros(size(CvecS));
    for ff=1:F
        T = kron(((Bt11+Bt12*S(:,:,ff))^-1).',(St(:,:,ff)*Bt12+Bt22));
        CvecSt(:,:,ff) = T*CvecS(:,:,ff)*(T');
    end
else
    CvecSt = [];
end

end


function mixedPorts = checkMixedPorts(mixedPorts,N,M,p)
% checks the mixedPorts for anomalies.

% check whether it is a 2xM matrix, if it's a Mx2 matrix, permute, warn and continue
if M~=2
    if p==2
        mixedPorts = mixedPorts';
        warning('mixedPorts has been permuted. it should be a 2xM matrix');%#ok
        p=M;
        M=2;
    else
        error('the MixedPorts matrix should be 2xM, you can only combine 2 ports at a time.')
    end
end
if p>N/2
    error('the amount of ports to combine cannot exceed the total amount of ports');
end
if any(find(mixedPorts>N))
    error('all elements in mixedPorts should be smaller than the size of the S-matrix');
end
if any(find(mixedPorts<1))
    error('all elements in mixedPorts should be larger than 1');
end
if length(unique(mixedPorts(:)))~=2*p
    error('every port can only be in mixedPorts once.');
end
if ~isempty(setdiff(mixedPorts(:),round(mixedPorts(:))))
    error('all ports should have integer numbers.');
end

end